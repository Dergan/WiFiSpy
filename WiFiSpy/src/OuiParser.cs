﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WiFiSpy.src
{
    public class OuiParser
    {
        private static SortedList<int, string> OuiNames;

        public static void Initialize(string FilePath)
        {
            OuiNames = new SortedList<int, string>();

            if (!File.Exists(FilePath))
                return;
            
            string Data = File.ReadAllText(FilePath);
            string pattern = @"([a-zA-Z0-9]{2})-([a-zA-Z0-9]{2})-([a-zA-Z0-9]{2})[ ]*\(hex\)[ \t]*([a-zA-Z0-9 ]*)";

            foreach (Match match in Regex.Matches(Data, pattern))
            {
                byte FirstByte = Convert.ToByte(match.Groups[1].Value, 16);
                byte SecondByte = Convert.ToByte(match.Groups[2].Value, 16);
                byte ThirdByte = Convert.ToByte(match.Groups[3].Value, 16);
                string Name = match.Groups[4].Value;

                int IntAddr = FirstByte | SecondByte << 8 | ThirdByte << 16;

                if (!OuiNames.ContainsKey(IntAddr))
                {
                    OuiNames.Add(IntAddr, Name);
                }
            }
        }

        public static string GetOuiByMac(byte[] MacAddress)
        {
            lock (OuiNames)
            {
                if (MacAddress != null && MacAddress.Length >= 3)
                {
                    int IntAddr = MacAddress[0] | MacAddress[1] << 8 | MacAddress[2] << 16;
                    string ret = "";
                    if (!OuiNames.TryGetValue(IntAddr, out ret))
                        return "";
                    return ret;
                }
                return "";
            }
        }
    }
}